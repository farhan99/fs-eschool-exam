<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class DataGuruController extends BaseController
{
    public function index()
    {
        $data['judul'] = 'data_guru';
        $data['url'] = 'data_guru';
        $data['model'] = $this->modelDataGuru
            //->join('uang_sekolah', 'uang_sekolah.id_uang_sekolah = siswa.spp', 'LEFT')
            ->findAll();
        echo view('/data_guru/index', $data);
    }

    public function tambah()
    {
        $id = 0;
        $data['judul'] = 'Tambah DataGuru';
        $data['url'] = 'data-guru/tambah';
        $data['id'] = $id;
        $data['model'] = $this->modelDataGuru->find($id);
        //$data['modelUangSekolah'] = $this->modelUangSekolah
        //->join('kategori', 'kategori.id_kategori = uang_sekolah.id_kategori', 'LEFT')
        //->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = uang_sekolah.id_tahun_ajaran', 'LEFT')
        //->like('kategori.nama_kategori', 'SPP', 'after')
        //->findAll();
        return view('/data_guru/form', $data);
    }

    public function edit()
    {
        $id = $this->req->uri->getSegment(3);
        $data['judul'] = 'Edit Data Guru';
        $data['url'] = 'data_guru/edit';
        $data['id'] = $id;
        $data['model'] = $this->modelDataGuru->find($id);
        //$data['modelUangSekolah'] = $this->modelUangSekolah
        // ->join('kategori', 'kategori.id_kategori = uang_sekolah.id_kategori', 'LEFT')
        // ->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = uang_sekolah.id_tahun_ajaran', 'LEFT')
        // ->like('kategori.nama_kategori', 'SPP', 'after')
        // ->findAll();
        echo view('/data_guru/form', $data);
    }

    public function insertData()
    {
        $data = array(
            'nip' => $this->request->getPost('nip'),
            'nama_guru' => $this->request->getPost('nama_guru'),
            'tempat_lahir' => $this->request->getPost('tempat_lahir'),
            'tgl_lahir' => $this->request->getPost('tgl_lahir'),
            'no_telp' => $this->request->getPost('no_telp'),
            'alamat' => $this->request->getPost('alamat'),
            'foto_guru' => '',
        );

        $this->modelDataGuru->save($data);
        return redirect('data-guru');
    }

    public function updateData()
    {
        $data = array(
            'id_guru' => $this->request->getPost('id_guru'),
            'nip' => $this->request->getPost('nip'),
            'nama_guru' => $this->request->getPost('nama_guru'),
            'tempat_lahir' => $this->request->getPost('tempat_lahir'),
            'tgl_lahir' => $this->request->getPost('tgl_lahir'),
            'no_telp' => $this->request->getPost('no_telp'),
            'alamat' => $this->request->getPost('alamat'),
            'foto_guru' => '',
        );

        $this->modelDataGuru->save($data);

        return redirect('data_guru');
    }

    public function deleteData()
    {
        $id = $this->req->uri->getSegment(3);

        if ($this->modelDataGuru->delete($id)) {

            return redirect('data_guru');
        }
    }
}
