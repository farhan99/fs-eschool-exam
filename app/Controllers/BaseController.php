<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;


use Config\Services;

use App\Models\DataGuru_model;
use App\Models\DataMapel_model;
use App\Models\Evaluasi_model;
use App\Models\Kelas_model;
use App\Models\KelasMapel_model;
use App\Models\Siswa_model;
use App\Models\User_model;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * Instance of the main Request object.
	 *
	 * @var IncomingRequest|CLIRequest
	 */
	protected $request;

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();

		$this->req = Services::request();
		$this->session = Services::session();


		$this->modelDataGuru = new DataGuru_model();
		$this->modelDataMapel = new DataMapel_model();
		$this->modelEvaluasi = new Evaluasi_model();
		$this->modelKelas = new Kelas_model();
		$this->modelKelasMapel = new KelasMapel_model();
		$this->modelSiswa = new Siswa_model();
		$this->modelUser = new User_model();
	}
}
