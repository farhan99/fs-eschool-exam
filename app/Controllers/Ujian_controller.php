<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class Ujian_controller extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Evaluasi',
            'url' => 'evaluasi',
            'siswa_model' => $this->modelSiswa->findAll(),
            'mapel_model' => $this->modelDataMapel->findAll(),
            'kelas_model' => $this->modelKelas->join('data_guru', 'data_guru.nip = kelas.nip', 'LEFT')->findAll()
        ];

        return view('/evaluasi/index', $data);
    }

    public function buat_soal()
    {
        $data = [
            'judul' => 'Form Buat Soal',
            'kelas_model' => $this->modelKelas->findAll()
        ];
        // $data['kelas_model'] = $this->modelKelas->find($id);
        echo view('/ujian/form-buat-soal', $data);
    }
}
