<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			'judul' => "Dashboard | eSchool"
		];
		return view('dashboard', $data);
	}
}
