<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class EvaluasiMapel_controller extends BaseController
{
    public function index()
    {
        $data = [
            'judul' => 'Evaluasi',
            'url' => 'evaluasi',
            'siswa_model' => $this->modelSiswa->findAll(),
            'kelas_model' => $this->modelKelas->join('data_guru', 'data_guru.nip = kelas.nip', 'LEFT')->findAll()
        ];

        return view('/evaluasi/index', $data);
    }

    public function daftar_mapel()
    {
        // $id = 1;
        $id = $this->req->uri->getSegment(3);
        $data = [
            'judul'  => 'Daftar Mapel',
            'url' => 'evaluasi',
            'kelasMapel_model' => $this->modelKelasMapel->join('data_mata-pelajaran', 'data_mata-pelajaran.id_mapel = kelas_mapel.id_mapel', 'LEFT')
                ->join('kelas', 'kelas.id_kelas = kelas_mapel.id_kelas', 'LEFT')
                ->join('data_guru', 'data_guru.nip = kelas_mapel.nip', 'LEFT')
                ->where('kelas.id_kelas', $id)
                ->findAll(),
            'namaKelas' => $this->modelKelas->where('id_kelas', $id)->findAll(),
            'mapel_model' => $this->modelDataMapel->findAll(),
        ];

        return view('/evaluasi/mapel_view', $data);
    }
}
