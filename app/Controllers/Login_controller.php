<?php

namespace App\Controllers;

use App\Models\User_model;

class Login_controller extends BaseController
{
    protected $helpers = [];

    public function index()
    {
        // if (isset(session('userData')['level_user'])) {
        //     return redirect()->to(base_url() . '/');
        // }
        $this->session->set('isLoggedIn', false);
        $this->session->set('userData', array());
        return view('login');
    }

    public function login()
    {

        $users = new User_model();
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $dataUser = $users->where([
            'username' => $username,
        ])->first();
        if ($dataUser) {
            if (password_verify($password, $dataUser->password)) {
                session()->set([
                    'username' => $dataUser->username,
                    'name' => $dataUser->name,
                    'logged_in' => TRUE
                ]);
                return redirect()->to(base_url('dashboard'));
            } else {
                session()->setFlashdata('error', 'Username & Password Salah');
                return redirect()->back();
            }
        } else {
            session()->setFlashdata('error', 'Username & Password Salah');
            return redirect()->back();
        }
    }

    public function register()
    {
        return view('register');
    }

    public function save()
    {
        if (!$this->validate([
            'username' => [
                'rules' => 'required|alpha_numeric|max_length[20]|is_unique[user.username]',
                'errors' => [
                    'required' => '{field} tidak boleh kosong',
                    'alpha_numeric' => '{field} harus berupa huruf atau angka',
                    'max_length' => '{field} maksimal 20 karakter',
                    'is_unique' => 'Username sudah terdaftar sebelumnya'
                ]
            ],
            'name' => [
                'rules' => 'required|alpha_space|max_length[32]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'alpha_space' => 'nama tidak boleh mengandung karakter lain',
                    'max_length' => '{field} maksimal 32 karakter',
                ]
            ],
            'password' => [
                'rules' => 'required|alpha_numeric_punct|min_length[6]|max_length[50]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'min_length' => '{field} minimal 6 karakter',
                    'max_length' => '{field} maksimal 50 karakter',
                ]
            ],
            'confirm' => [
                'rules' => 'matches[password]',
                'errors' => [
                    'matches' => 'Konfirmasi password tidak cocok',
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        $users = new User_model();
        $users->insert([
            'username' => $this->request->getVar('username'),
            'name' => $this->request->getVar('name'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
        ]);
        return redirect()->to('/');
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }
}
