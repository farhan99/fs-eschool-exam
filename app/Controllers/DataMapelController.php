<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class DataMapelController extends BaseController
{
	public function index()
	{
        $data['judul'] = 'DataMapel';
        $data['url'] = 'data-mapel';
		$data['model'] = $this->modelDataMapel->findAll();
            // ->join('uang_sekolah', 'uang_sekolah.id_uang_sekolah = siswa.spp', 'LEFT')
            // ->findAll();
		echo view('/data_mapel/index', $data); 
	}

    public function tambah()
    {
        $id = 0;
        $data['judul'] = 'Tambah Siswa';
        $data['url'] = 'siswa/tambah';
        $data['id'] = $id;
        $data['model'] = $this->modelSiswa->find($id);
        $data['modelUangSekolah'] = $this->modelUangSekolah
            ->join('kategori', 'kategori.id_kategori = uang_sekolah.id_kategori', 'LEFT')
            ->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = uang_sekolah.id_tahun_ajaran', 'LEFT')
            ->like('kategori.nama_kategori', 'SPP', 'after')
            ->findAll();
        return view('/kelas_mapel/form', $data);
    }

    public function edit()
	{
		$id = $this->req->uri->getSegment(3);
        $data['judul'] = 'Edit Laporan Harian Kerja';
        $data['url'] = 'siswa/edit';
        $data['id'] = $id;
		$data['model'] = $this->modelSiswa->find($id);
        $data['modelUangSekolah'] = $this->modelUangSekolah
            ->join('kategori', 'kategori.id_kategori = uang_sekolah.id_kategori', 'LEFT')
            ->join('tahun_ajaran', 'tahun_ajaran.id_tahun_ajaran = uang_sekolah.id_tahun_ajaran', 'LEFT')
            ->like('kategori.nama_kategori', 'SPP', 'after')
            ->findAll();
		echo view('/kelas_mapel/form', $data);
	}

	public function insertData()
    {    	
     	$data = array(
     		'nis' => $this->request->getPost('nis'),
            'nisn' => $this->request->getPost('nisn'),
            'nama_siswa' => $this->request->getPost('nama_siswa'),
            'tempat_lahir' => $this->request->getPost('tempat_lahir'),
            'tanggal_lahir' => $this->request->getPost('tanggal_lahir'),
            'no_telp' => $this->request->getPost('no_telp'),
            'spp' => $this->request->getPost('spp'),
        );

     	$this->modelSiswa->save($data);
		return redirect('siswa');
    }

    public function updateData()
    {
     	$data = array(
     		'id_siswa' => $this->request->getPost('id_siswa'),
            'nis' => $this->request->getPost('nis'),
            'nisn' => $this->request->getPost('nisn'),
            'nama_siswa' => $this->request->getPost('nama_siswa'),
            'tempat_lahir' => $this->request->getPost('tempat_lahir'),
            'tanggal_lahir' => $this->request->getPost('tanggal_lahir'),
            'no_telp' => $this->request->getPost('no_telp'),
            'spp' => $this->request->getPost('spp'),
        );

        $this->modelSiswa->save($data);

        return redirect('siswa');
    }

    public function deleteData()
    {
    	$id = $this->req->uri->getSegment(3);

    	if ($this->modelSiswa->delete($id)) {
		
			return redirect('kelas_mapel');
    	
    	}
    }

}
