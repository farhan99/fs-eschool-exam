<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class EvaluasiTugas_controller extends BaseController
{
    public function index()
    {
        $id = $this->req->uri->getSegment(3);
        $data = [
            'judul' => 'Tugas / Evaluasi',
            'url' => 'tugas',
            'kelasMapel_model' => $this->modelKelasMapel
                ->join('data_guru', 'data_guru.nip = kelas_mapel.nip', 'LEFT')
                ->join('kelas', 'kelas.id_kelas = kelas_mapel.id_kelas', 'LEFT')
                ->join('data_mata-pelajaran', 'data_mata-pelajaran.id_mapel = kelas_mapel.id_mapel', 'LEFT')
                ->where('kelas_mapel.id_kelas_mapel', $id)
                ->findAll(),
            'evaluasi_model' => $this->modelEvaluasi
                ->where('ex_evaluasi.id_kelas_mapel', $id)
                ->findAll()
        ];

        return view('/evaluasi/evaluasi_view', $data);
    }

    public function add()
    {
        // $id = 1;
        $id = $this->req->uri->getSegment(3);
        $data = [
            'judul'  => 'Tambah Tugas/Evaluasi',
            'url' => 'add',
            'kelasMapel_model' => $this->modelKelasMapel
                ->join('data_guru', 'data_guru.nip = kelas_mapel.nip', 'LEFT')
                ->join('kelas', 'kelas.id_kelas = kelas_mapel.id_kelas', 'LEFT')
                ->join('data_mata-pelajaran', 'data_mata-pelajaran.id_mapel = kelas_mapel.id_mapel', 'LEFT')
                ->where('kelas_mapel.id_kelas_mapel', $id)
                ->findAll(),
            'evaluasi_model' => $this->modelEvaluasi
                ->findAll(),
        ];
        return view('/evaluasi/addTugas_view', $data);
    }

    public function save()
    {
        $id = $this->req->uri->getSegment(3);
        $data = [
            'jenis_evaluasi' => $this->request->getPost('jenis_evaluasi'),
            'id_kelas_mapel' => $this->request->getVar('idkelasmapel')
        ];
        session()->setFlashData('pesan', 'Data berhasil ditambahkan');
        $this->modelEvaluasi->save($data);

        return redirect()->to('/evaluasi/tugas/' . $id);
    }

    public function edit($id)
    {
        $id_mapel = $this->req->uri->getSegment(3);
        $data = [

            'judul' => 'Form Edit Tugas',
            'evaluasi' => $this->modelEvaluasi->find($id),
            'kelasMapel_model' => $this->modelKelasMapel
                ->join('data_guru', 'data_guru.nip = kelas_mapel.nip', 'LEFT')
                ->join('kelas', 'kelas.id_kelas = kelas_mapel.id_kelas', 'LEFT')
                ->join('data_mata-pelajaran', 'data_mata-pelajaran.id_mapel = kelas_mapel.id_mapel', 'LEFT')
                ->where('kelas_mapel.id_kelas_mapel', $id_mapel)
                ->findAll(),
            'evaluasi_model' => $this->modelEvaluasi
                ->findAll(),
        ];
        return view('/evaluasi/editTugas_view', $data);
    }

    public function update()
    {
        $id_mapel = $this->req->uri->getSegment(3);
        $data = [
            'id_evaluasi' => $this->request->getVar('idEvaluasi'),
            'jenis_evaluasi' => $this->request->getPost('jenis_evaluasi'),
            'id_kelas_mapel' => $this->request->getVar('idkelasmapel')
        ];
        $this->modelEvaluasi->save($data);
        session()->setFlashData('pesan', 'Data berhasil diubah');
        // dd($data);
        return redirect()->to('/evaluasi/tugas/' . $id_mapel);
    }

    public function delete($id)
    {
        $id_evaluasi = $this->req->uri->getSegment(3);
        $this->modelEvaluasi->delete($id);
        session()->setFlashData('pesan', 'Data berhasil dihapus');

        return redirect()->to('/evaluasi/tugas/' . $id_evaluasi);
    }
}
