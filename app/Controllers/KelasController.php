<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class KelasController extends BaseController
{
	public function index()
	{
        $data['judul'] = 'Kelas';
        $data['url'] = 'kelas';
		$data['model'] = $this->modelKelas->findAll();
		echo view('/kelas/index', $data); 
	}

    public function tambah()
    {
        $id = 0;
        $data['judul'] = 'Tambah Laporan Harian Kerja';
        $data['url'] = 'kelas/tambah';
        $data['id'] = $id;
        $data['model'] = $this->modelKelas->find($id);
        return view('/kelas/form', $data);
    }

    public function edit()
	{
		$id = $this->req->uri->getSegment(3);
        $data['judul'] = 'Edit Laporan Harian Kerja';
        $data['url'] = 'kelas/edit';
        $data['id'] = $id;
		$data['model'] = $this->modelKelas->find($id);
		echo view('/kelas/form', $data);
	}

	public function insertData()
    {    	
     	$data = array(
     		'nama_kelas' => $this->request->getPost('nama_kelas'),
            'nip' => $this->request->getPost('nip'),
        );

     	$this->modelKelas->save($data);
		return redirect('kelas');
    }

    public function updateData()
    {
     	$data = array(
     		'id_kelas' => $this->request->getPost('id_kelas'),
            'nama_kelas' => $this->request->getPost('nama_kelas'),
            'nip' => $this->request->getPost('nip'),
        );

        $this->modelKelas->save($data);

        return redirect('kelas');
    }

    public function deleteData()
    {
    	$id = $this->req->uri->getSegment(3);

    	if ($this->modelKelas->delete($id)) {
		
			return redirect('kelas');
    	
    	}
    }

}
