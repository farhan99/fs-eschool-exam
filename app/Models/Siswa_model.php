<?php

namespace App\Models;

use CodeIgniter\Model;

class Siswa_model extends Model
{
    protected $table = 'siswa';
    protected $primaryKey = 'id_siswa';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields =
    [
        'nis',
        'nisn',
        'nama_siswa',
        'tempat_lahir',
        'tanggal_lahir',
        'no_telp',
        'alamat',
        'jenis_kelamin',
        'foto_siswa',
        'spp',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];

    protected $dynamicRules = [];

    protected $skipValidation = false;
}
