<?php

namespace App\Models;

use CodeIgniter\Model;

class KelasMapel_model extends Model
{
    protected $table = 'kelas_mapel';
    protected $primaryKey = 'id_kelas_mapel';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields =
    [
        'id_mapel',
        'id_kelas',
        'nip',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];

    protected $dynamicRules = [];

    protected $skipValidation = false;
}
