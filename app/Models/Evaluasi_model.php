<?php

namespace App\Models;

use CodeIgniter\Model;

class Evaluasi_model extends Model
{
    protected $table = 'ex_evaluasi';
    protected $primaryKey = 'id_evaluasi';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields =
    [
        'id_evaluasi',
        'jenis_evaluasi',
        'id_kelas_mapel',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];

    protected $dynamicRules = [];

    protected $skipValidation = false;
}
