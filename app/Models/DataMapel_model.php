<?php

namespace App\Models;

use CodeIgniter\Model;

class DataMapel_model extends Model
{
    protected $table = 'data_mata-pelajaran';
    protected $primaryKey = 'id_mapel';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields =
    [
        'nama_mapel',

    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];

    protected $dynamicRules = [];

    protected $skipValidation = false;
}
