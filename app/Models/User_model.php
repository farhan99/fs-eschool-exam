<?php

namespace App\Models;

use CodeIgniter\Model;

class User_model extends Model
{
    protected $table = 'user';
    protected $primaryKey = 'id_user';
    protected $useAutoIncrement = true;

    protected $returnType = 'object';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['username', 'name', 'password', 'privilege', 'status', 'accessed_at'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';
    // protected $dateFormat   = 'datetime';

    // protected $validationRules = [];

    // protected $dynamicRules = [
    //     'insertData' => [
    //         'username'  => 'required|alpha_numeric|is_unique[user.username, id_user, {id_user}]',
    //         'name'      => 'required|alpha_space',
    //         'password'  => 'required|alpha_numeric_punct|min_length[6]',
    //         'confirm'   => 'matches[password]'
    //     ],
    // ];
    // protected $validationMessages = [
    //     'username' => [
    //         'alpha_numeric' => 'Username hanya boleh mengandung huruf dan angka',
    //         'is_unique' => 'Username sudah digunakan'
    //     ],
    //     'password' => [
    //         'min_length' => 'Password harus terdiri dari min. 6 kata',
    //         'alpha_numeric_punct' => 'Password hanya boleh mengandung angka, huruf, dan karakter yang valid'
    //     ],
    //     'confirm' => [
    //         'matches' => 'Konfirmasi password tidak cocok'
    //     ]
    // ];
    // protected $skipValidation = false;

    // protected $beforeInsert = ['hashPassword'];
    // protected $beforeUpdate = ['hashPassword'];

    // public function getRule(string $rule)
    // {
    //     return $this->dynamicRules[$rule];
    // }

    // protected function hashPassword(array $data)
    // {
    //     if (!isset($data['data']['password'])) return $data;

    //     $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
    //     unset($data['data']['password']);

    //     return $data;
    // }
}
