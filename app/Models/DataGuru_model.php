<?php

namespace App\Models;

use CodeIgniter\Model;

class DataGuru_model extends Model
{
    protected $table = 'data_guru';
    protected $primaryKey = 'id_guru';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields =
    [
        'nip',
        'nama_guru',
        'tempat_lahir',
        'tgl_lahir',
        'no_telp',
        'alamat',
        'foto_guru',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedFiled = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];

    protected $dynamicRules = [];

    protected $skipValidation = false;
}
