<?= $this->extend('layout/menu-login') ?>
<?= $this->section('login-menu') ?>

<div class="login-title"><strong>Assalamu'alaikum</strong>, Silahkan Daftar</div>
<form action="<?= base_url() ?>/register" class="form-horizontal" method="POST">
    <?= csrf_field() ?>
    <div class="form-group">
        <?php if (!empty(session()->getFlashdata('error'))) : ?>
            <!-- <div class="alert alert-warning alert-dismissible fade show" role="alert"> -->
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <h4>Registrasi Gagal!</h4>
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
            </div>
        <?php endif; ?>
        <!-- <div class="col-md-12"> -->
        <!-- <//?php if (isset($validation)) : ?> -->
        <!-- <p class="alert alert-danger"><//?= $validation->listErrors() ?></p> -->
        <!-- <//?php endif; ?> -->
        <!-- </div> -->
        <div class="col-md-12">
            <input type="text" class="form-control" name="username" placeholder="Username" required />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <input type="text" class="form-control" name="name" placeholder="Full Name" required />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <input type="password" name="password" class="form-control" placeholder="Password" required />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <input type="password" name="confirm" class="form-control" placeholder="Confirm Password" required />
        </div>
    </div>
    <hr style="opacity: .2">
    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-warning btn-block">Register</button>
        </div>
        <div class="col-md-12 mt-4 text-center" style="margin-top: .8rem">
            <a href="<?= base_url(); ?>/" style="color:#fff">Sudah punya akun?</a>
            <!-- <button type="button" class="btn btn-warning btn-block">Register</button> -->
        </div>
        <p style="margin-top:2rem; background: red" align="right">
        </p>
    </div>
</form>

<?= $this->endSection() ?>