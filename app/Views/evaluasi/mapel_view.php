<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            <!-- <form class="form-horizontal" method="POST" action="<//?= base_url('/' . $url) ?>"> -->
            <form class="form-horizontal" method="POST" action="">
                <?= csrf_field() ?>
                <div class="row" style="margin: .6rem 0">
                    <a href="<?= base_url(); ?>/evaluasi" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <!-- <h3 class="panel-title"><strong>MATA PELAJARAN | Kelas: <//?= var_dump($namaKelas); ?></strong></h3> -->
                        <h3 class="panel-title"><strong>MATA PELAJARAN</strong></h3>
                        <h5 class="panel-title"><strong>Kelas: <?= $kelasMapel_model[0]['nama_kelas']; ?></strong></h5>
                        <br>
                        <hr style="margin-bottom: 0px;">
                        <!-- <select class="form-select" aria-label="Default select example">
                            <option selected> --Pilih Ujian Mata Pelajaran-- </option>
                            <//?php
                            $no_mapel = 0;
                            foreach ($mapel_model as $m) :
                                $no_mapel++;
                            ?>
                                <option value="$no_mapel"><//?= $m['nama_mapel'] ?></option>
                                <!-- <option value="1">Bahasa Indonesia</option>
                                <option value="2">Matematika</option>
                                <option value="3">Bahasa Inggris</option>
                                <option value="3">IPA</option>
                                <option value="3">IPS</option> -->
                        <!-- <//?php endforeach; ?> -->
                        <!-- </select> -->
                    </div>

                    <div class="panel-body panel-body-table table-responsive">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                    <tr>
                                        <th width="50">No</th>
                                        <th width="50">tmp_id-kelasMapel</th>
                                        <!-- <th width="50">tmp_id-kelas</th> -->
                                        <!-- <th width="100">tmp_nama-kelas</th> -->
                                        <th width="20%">Mata Pelajaran</th>
                                        <th width="50%">Guru / Pengajar</th>
                                        <th>actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($kelasMapel_model as $kMp) :
                                        $no++;
                                    ?>
                                        <tr id="trow_<?= $no ?>">
                                            <td class="text-center"><?= $no ?></td>
                                            <td class="text-center"><?= $kMp['id_kelas_mapel'] ?></td>
                                            <!-- <td class="text-center"><//?= $kMp['id_kelas'] ?></td> -->
                                            <!-- <td class="text-center"><//?= $kMp['nama_kelas'] ?></td> -->
                                            <td><strong style="text-transform: uppercase"><?= $kMp['nama_mapel'] ?></strong></td>
                                            <td><strong style="text-transform: capitalize"><?= $kMp['nama_guru'] ?></strong></td>
                                            <td align="center">
                                                <!-- <a href="<..?= base_url() . '/' . $url; ?>/kelas/<..?= $kMp['id_kelas_mapel']; ?>" class="btn btn-default btn-info  btn-sm"><span class="fa fa-pencil-square"></span>TUGAS / EVALUASI</a> -->
                                                <a href="<?= base_url() . '/' . $url; ?>/tugas/<?= $kMp['id_kelas_mapel']; ?>" class="btn btn-default btn-info  btn-sm"><span class="fa fa-pencil-square"></span>TUGAS / EVALUASI</a>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </div>

</div>


<!-- END PAGE CONTENT WRAPPER -->
<?= $this->endSection() ?>