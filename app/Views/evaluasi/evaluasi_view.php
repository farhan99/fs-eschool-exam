<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>
<!-- PAGE CONTENT WRAPPER -->
<style>
    .btn-soal {
        padding-right: 1.5rem;
    }

    .msg-success {
        background-color: rgba(98, 216, 126, 0.47);
        border-radius: 6px;
        color: #000;
        font-weight: 600;
        float: right;
        text-transform: uppercase;
    }
</style>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">

            <!-- <form class="form-horizontal" method="POST" action="<//?= base_url('/' . $url) ?>"> -->
            <!-- <a href="<?= base_url(); ?>/evaluasi/mapel/<//?= $i['id_kelas']; ?>" class="button">Kembali</a> -->
            <div class="row" style="margin: .6rem 0">
                <!-- <div class="col-md-1"> -->
                <a href="<?= base_url(); ?>/evaluasi/mapel/<?= $kelasMapel_model[0]['id_kelas']; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>Kembali</a>
                <!-- </div> -->
                <!-- <div class="col-md-11"> -->
                <?php if (!empty(session()->getFlashdata('pesan'))) : ?>
                    <span class="btn msg-success"><?= session()->getFlashdata('pesan'); ?></span>
                <?php endif; ?>
                <!-- </div> -->
            </div>
            <?= csrf_field() ?>
            <div class="panel panel-default" style="margin: 0">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>DAFTAR EVALUASI</strong></h3>
                    <br>
                    <hr style="margin-bottom: 0px;">
                </div>
                <div class="panel-body panel-body-table table-responsive">
                    <div class="table-responsive">
                        <table border="0" width="100%">
                            <tr style="padding: 2px 0">
                                <td width="20%">
                                    <h5 class=""><strong>Mata Pelajaran</strong></h5>
                                </td>
                                <td width="1.5%">:</td>
                                <td style="text-transform: capitalize"><?= $kelasMapel_model[0]['nama_mapel']; ?></td>
                                <td class="btn-soal" align="right" width="15%" rowspan="2"><a href="<?= base_url(); ?>/evaluasi/tugas/<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>/add?idkelasmapel=<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>" class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i>
                                        Tambah Tugas</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <h5 class=""><strong>Guru Pengajar</strong></h5>
                                </td>
                                <td>:</td>
                                <td style="text-transform: uppercase"> <?= $kelasMapel_model[0]['nama_guru']; ?></td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                                <tr>
                                    <th width="50">No</th>
                                    <th width="60%">Jenis Tugas / Evaluasi</th>
                                    <th>actions</th>
                                    <th>Soal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 0;
                                foreach ($evaluasi_model as $ev) :
                                    $no++;
                                ?>
                                    <tr id="trow_<?= $no ?>">
                                        <td class="text-center"><?= $no ?></td>
                                        <td><strong style="text-transform: uppercase"><?= $ev['jenis_evaluasi']; ?></strong></td>
                                        <td align="center">
                                            <!-- TOMBOL EDIT -->
                                            <a href="<?= base_url() . '/evaluasi/' . $url; ?>/<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>/edit/<?= $ev['id_evaluasi']; ?>" class="btn btn-default btn-info  btn-sm"><span class="fa fa-pencil-square"></span>Edit</a>
                                            <!-- TOMBOL HAPUS -->
                                            <a href="<?= base_url() . '/evaluasi/' . $url; ?>/<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>/delete/<?= $ev['id_evaluasi']; ?>" class="btn btn-default btn-danger  btn-sm" onclick="return confirm('Hapus data ini?');"><span class="fa fa-trash"></span>Hapus</a>
                                            <!-- <a class="btn btn-danger btn-rounded btn-condensed btn-sm" href="<?= base_url() . '/' . $url; ?>/delete/<//?= $s['id_siswa'] ?>" onClick="return confirm('Hapus Data Ini ?')"><span class="fa fa-times"></span></a> -->
                                        </td>
                                        <td align="center">
                                            <a href="<?= base_url() . '/' . $url; ?>" class="btn btn-default btn-success  btn-sm"><span class="fa fa-pencil-square-o"></span> Buat Soal</a>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- </form> -->

        </div>
    </div>
</div>

<!-- END PAGE CONTENT WRAPPER -->
<?= $this->endSection() ?>