<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">


            <!-- <form class="form-horizontal" method="POST" action="<//?= base_url('/' . $url) ?>"> -->
            <form class="form-horizontal" method="POST" action="">
                <?= csrf_field() ?>
                <div class="panel panel-default" style="margin: 0">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>DAFTAR KELAS</strong></h3>
                        <br>
                        <hr style="margin-bottom: 0px;">
                        <!-- <select class="form-select" aria-label="Default select example">
                            <option selected> --Pilih Ujian Mata Pelajaran-- </option>
                            <//?php
                            $no_mapel = 0;
                            foreach ($mapel_model as $m) :
                                $no_mapel++;
                            ?>
                                <option value="$no_mapel"><//?= $m['nama_mapel'] ?></option>
                                <!-- <option value="1">Bahasa Indonesia</option>
                                <option value="2">Matematika</option>
                                <option value="3">Bahasa Inggris</option>
                                <option value="3">IPA</option>
                                <option value="3">IPS</option> -->
                        <!-- <//?php endforeach; ?> -->
                        <!-- </select> -->
                    </div>
                    <div class="panel-body panel-body-table table-responsive">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                    <tr>
                                        <th width="50">No</th>
                                        <th width="20%">Kelas</th>
                                        <th width="50%">Wali Kelas</th>
                                        <th>actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($kelas_model as $k) :
                                        $no++;
                                    ?>
                                        <tr id="trow_<?= $no ?>">
                                            <td class="text-center"><?= $no ?></td>
                                            <td><strong style="text-transform: uppercase"><?= $k['nama_kelas'] ?></strong></td>
                                            <td><strong style="text-transform: capitalize"><?= $k['nama_guru'] ?></strong></td>
                                            <td align="center">
                                                <!-- <a href="" class="btn btn-default btn-success btn-sm"><span class="fa fa-leanpub"></span> e-Learning</a> -->
                                                <a href="<?= base_url() . '/' . $url; ?>/mapel/<?= $k['id_kelas']; ?>" class="btn btn-default btn-warning  btn-sm"><span class="fa fa-pencil-square"></span>Dafar Mata Pelajaran</a>
                                                <!-- <a href="<//?= base_url() . '/' . $url; ?>/edit/<? //= $s['id_siswa'] 
                                                                                                        ?>" class="btn btn-default btn-rounded btn-condensed btn-sm"><span class="fa fa-pencil"></span></a> -->

                                                <!-- <a class="btn btn-danger btn-rounded btn-condensed btn-sm" href="<?= base_url() . '/' . $url; ?>/delete/<//?= $s['id_siswa'] ?>" onClick="return confirm('Hapus Data Ini ?')"><span class="fa fa-times"></span></a> -->
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>


<!-- END PAGE CONTENT WRAPPER -->
<?= $this->endSection() ?>