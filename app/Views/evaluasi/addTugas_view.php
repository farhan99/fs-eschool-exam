<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>
<!-- PAGE CONTENT WRAPPER -->
<style>
    .btn-soal {
        padding-right: 1.5rem;
    }
</style>
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">


            <!-- <form class="form-horizontal" method="POST" action="<//?= base_url('/' . $url) ?>"> -->
            <!-- <form class="form-horizontal" method="POST" action=""> -->
            <!-- <a href="<?= base_url(); ?>/evaluasi/mapel/<//?= $i['id_kelas']; ?>" class="button">Kembali</a> -->
            <div class="row" style="margin: .6rem 0">
                <a href="<?= base_url(); ?>/evaluasi/tugas/<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>" class="btn btn-default"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                    Kembali</a>
            </div>
            <div class="panel panel-default" style="margin: 20">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>TAMBAH TUGAS/EVALUASI</strong></h3>
                    <br>
                    <hr style="margin-bottom: 0px;">
                </div>
                <div class="panel-body panel-body-table table-responsive" style="margin-bottom: 20px">
                    <div class="table-responsive">
                        <table border="0" width="100%">
                            <tr style="padding: 2px 0">
                                <td width="20%">
                                    <h5 class=""><strong>Mata Pelajaran</strong></h5>
                                </td>
                                <td width="1.5%">:</td>
                                <td style="text-transform: capitalize"><?= $kelasMapel_model[0]['nama_mapel']; ?></td>
                                <!-- <td class="btn-soal" align="right" width="15%" rowspan="2"><a href="<//?= base_url(); ?>/evaluasi/tugas/<//?= $kelasMapel_model[0]['id_kelas_mapel']; ?>/add" class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i>
                                        Tambah Tugas</a></td> -->
                            </tr>
                            <tr>
                                <td>
                                    <h5 class=""><strong>Guru Pengajar</strong></h5>
                                </td>
                                <td>:</td>
                                <td style="text-transform: uppercase"> <?= $kelasMapel_model[0]['nama_guru']; ?></td>
                            </tr>
                        </table>
                        <hr style="margin-top: 0px;">
                        <div class="container" style="width: 90%;">
                            <form action="<?= base_url(); ?>/evaluasi/tugas/<?= $kelasMapel_model[0]['id_kelas_mapel']; ?>/add" class="form-horizontal" method="POST">
                                <?= csrf_field() ?>
                                <input type="hidden" id="idkelasmapel" name="idkelasmapel" value="<?= $_GET['idkelasmapel']; ?>">
                                <div class="form-group">
                                    <label for="jenis-evaluasi">Jenis Tugas/Evaluasi (misal: UH, UTS, UAS)</label>
                                    <input type="text" name="jenis_evaluasi" class="form-control" style="width: 60%;" id="jenis-evaluasi" required />
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-warning">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- </form> -->

        </div>
    </div>

</div>


<!-- END PAGE CONTENT WRAPPER -->
<?= $this->endSection() ?>