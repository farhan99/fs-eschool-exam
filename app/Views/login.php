<?= $this->extend('layout/menu-login') ?>
<?= $this->section('login-menu') ?>
<style>
    #login_message {
        text-align: center;
        padding: 5px;
        border-radius: 4px;
    }
</style>

<div class="login-title"><strong>Assalamu'alaikum</strong>, Silahkan login</div>
<form action="<?= base_url() ?>/login" class="form-horizontal" method="POST">
    <?= csrf_field() ?>
    <div class="form-group">
        <?php if (!empty(session()->getFlashdata('error'))) : ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
            </div>
        <?php endif; ?>
        <!-- <//?php if (session()->getFlashdata('register_success')) : ?> -->
        <!-- <div class="col-md-12">
                <p class="alert alert-success" id="login_message">
                    <//?= session()->get('register_success'); ?>
                </p>
            </div>
        <//?php endif; ?> -->
        <div class="col-md-12">
            <input type="text" class="form-control" name="username" placeholder="<?= lang('Auth.username') ?>" value="<?= old('username') ?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <input type="password" name="password" class="form-control" placeholder="<?= lang('Auth.password') ?>" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6">
            <button class="btn btn-info btn-block">Log In</button>
        </div>
        <div class="col-md-6">
            <a href="<?= base_url(); ?>/register" class="btn btn-warning btn-block">Register</a>
        </div>
    </div>
</form>

<?= $this->endSection() ?>