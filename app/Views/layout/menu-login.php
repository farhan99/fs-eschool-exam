<!DOCTYPE html>
<html lang="en" class="body-full-height">

<head>
    <!-- META SECTION -->
    <title>Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?= base_url() ?>/public/favicon.png" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?= base_url() ?>/public/css/theme-default.css" />
    <!-- <link rel="stylesheet" type="text/css" id="theme" href="<?= base_url() ?>/public/bootstrap-5.1.0-dist/bootstrap.css" /> -->

    <!-- EOF CSS INCLUDE -->


</head>

<body>
    <div class="login-container">
        <div class="login-box animated fadeInDown">
            <!-- <div class="login-logo"></div> -->
            <div class="login-body">
                <?= $this->renderSection('login-menu') ?>
            </div>
            <div class="login-footer">
                <div class="pull-right">
                    &copy; 2021 APK-UIN SUSKA
                </div>
            </div>
        </div>
    </div>

</body>

</html>