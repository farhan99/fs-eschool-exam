<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META SECTION -->
    <title><?= $judul ?></title>
    <!-- <title>eSchool | Keuangan</title> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?= base_url() ?>/public/favicon.png" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?= base_url() ?>/public/css/theme-default.css" />

    <!-- EOF CSS INCLUDE -->


    <!-- textarea editor -->
    <link rel="stylesheet" href="<?= base_url() ?>/public/assets/codemirror/lib/codemirror.min.css">
    <!-- <link rel="stylesheet" href="<?= base_url() ?>/public/assets/froala_editor/css/froala_editor.pkgd.min.css"> -->
    <!-- <link rel="stylesheet" href="<?= base_url() ?>/public/assets/froala_editor/css/froala_style.min.css"> -->
    <!-- <link rel="stylesheet" href="<?= base_url() ?>/public/assets/froala_editor/css/themes/royal.min.css"> -->
    <!-- /texarea editor; -->
</head>

<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <!-- START PAGE SIDEBAR -->
        <div class="page-sidebar">
            <!-- START X-NAVIGATION -->
            <ul class="x-navigation">
                <!-- <li class="xn-logo"> -->
                <li class="" style="background: #015391">
                    <a href="index.html">FS | eSchool </a>
                    <a class="x-navigation-control"></a>
                </li>
                <li class="xn-profile">
                    <a href="#" class="profile-mini">
                        <img src="<?= base_url() ?>/public/assets/images/users/avatar.jpg" alt="John Doe" />
                    </a>
                    <div class="profile">
                        <div class="profile-image">
                            <img src="<?= base_url() ?>/public/assets/images/users/avatar.jpg" alt="John Doe" />
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name"><?= session()->get('name'); ?></div>
                            <div>@<?= session()->get('username'); ?></div>
                            <div class="profile-data-title">EL HAQQA QUR'ANIC SCHOOL</div>
                        </div>
                        <div class="profile-controls">
                            <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                            <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                        </div>
                    </div>
                </li>
                <li class="xn-title">Menu</li>
                <li>
                    <a href="<?= base_url() ?>/dashboard"><span class="fa fa-dashboard"></span> <span class="xn-text">Home</span></a>
                </li>
                <!--                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">Data Master</span></a>
                        <ul>
                            <li><a href="<?php //base_url() 
                                            ?>/pegawai">Pegawai</a></li>
                            <li><a href="<?php // base_url() 
                                            ?>/jabatan">Jabatan</a></li>
                            <li><a href="<?php // base_url() 
                                            ?>/pangkat">Pangkat/Gol</a></li>
                            <li><a href="<?php // base_url() 
                                            ?>/user">Pengguna</a></li>
                        </ul>
                    </li> -->
                <!--                     <li>
                    <a href="<?php // base_url('/kontraksaya') 
                                ?>"><span class="fa fa-file"></span> <span class="xn-text">Kontrak Kerja Saya</span></a>
                    </li> -->

                <li class="xn-openable">
                    <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">Data Master</span></a>
                    <ul>
                        <li><a href="<?= base_url() ?>/kelas">Kelas</a></li>
                        <li><a href="<?= base_url() ?>/data-guru">Data Guru</a></li>
                        <li><a href="<?= base_url() ?>/data-mapel">Data Mata Pelajaran</a></li>
                        <li><a href="<?= base_url() ?>/siswa">Siswa</a></li>
                        <li><a href="<?= base_url() ?>/tahunajaran">Tahun Ajaran</a></li>
                        <li><a href="<?= base_url() ?>/kelas-mapel">Kelas Mapel</a></li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">ePayment</span></a>
                            <ul>
                                <li><a href="<?= base_url() ?>/kategori">Kategori</a></li>
                                <li><a href="<?= base_url() ?>/uangsekolah">Uang Sekolah</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">ePresensi</span></a>
                            <ul>
                                <li><a href="<?= base_url() ?>/kelas">-</a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">eAkreditasi</span></a>
                            <ul>
                                <li><a href="<?= base_url() ?>/kelas">Borang</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="xn-openable">
                    <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">ePayment</span></a>
                    <ul>
                        <li><a href="<?= base_url() ?>/kelas">Kelas</a></li>
                        <li><a href="<?= base_url() ?>/lhk">LHK</a></li>
                    </ul>
                </li>

                <li class="xn-openable">
                    <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">ePresensi</span></a>
                    <ul>
                        <li><a href="<?= base_url() ?>/kelas">Absen Guru</a></li>
                        <li><a href="<?= base_url() ?>/kelas">Absen Siswa</a></li>
                    </ul>
                </li>

                <li class="xn-openable">
                    <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">eAkreditasi</span></a>
                    <ul>
                        <li><a href="<?= base_url() ?>/kelas">Borang</a></li>
                    </ul>
                </li>
                <li>
                    <!-- <a href="#"><span class="fa fa-folder"></span> <span class="xn-text">eLearning</span></a> -->
                    <!-- <ul> -->
                    <!-- <li> -->
                    <a href="<?= base_url() ?>/evaluasi"><span class="fa fa-file-text-o"></span> <span class="xn-text">Evaluasi/Ujian</span></a>
                    <!-- </li> -->
                    <!-- </ul> -->
                </li>


                <!-- <li><a href="<//?= base_url(); ?>/logout" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> <span class="xn-text">Keluar</span></a></li> -->
                <li><a href="<?= base_url(); ?>/logout"><span class="fa fa-sign-out"></span> <span class="xn-text">Keluar</span></a></li>

            </ul>
            <!-- END X-NAVIGATION -->
        </div>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <!-- TOGGLE NAVIGATION -->
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <!-- END TOGGLE NAVIGATION -->

                <!-- POWER OFF -->
                <li class="xn-icon-button pull-right last">
                    <a href="#" title="Keluar"><span class="fa fa-sign-out"></span></a>
                </li>
                <!-- END POWER OFF -->

                <!-- LANG BAR -->

                <!-- END LANG BAR -->
            </ul>
            <!-- END X-NAVIGATION VERTICAL -->

            <!-- START BREADCRUMB -->
            <!--                 <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Kontrak Kerja</li>
                </ul> -->
            <!-- END BREADCRUMB -->

            <!-- Konten  -->
            <?= $this->renderSection('content') ?>

        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->


    <!-- START PRELOADS -->
    <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->

    <!-- START SCRIPTS -->
    <!-- START PLUGINS -->
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- END PLUGINS -->

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?= base_url() ?>/public/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END THIS PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    <script type="text/javascript" src="<?= base_url() ?>/public/js/settings.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>/public/js/plugins.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>/public/js/actions.js"></script>

    <!-- END THIS PAGE PLUGINS-->




    <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('#id_kontrak').change(function() {
                var id_kontrak = $(this).val();
                $.ajax({
                    url: "<?php echo base_url(); ?>/rubrik/find",
                    method: "POST",
                    data: {
                        id_kontrak: id_kontrak
                    },
                    async: false,
                    dataType: 'json',
                    success: function(data) {
                        var html = '';
                        var i;

                        var select = document.getElementById('id_rubrik');

                        for (i = 0; i < data.length; i++) {

                            // html += '<option>'+data[i]['rincian_rubrik']+'</option>';
                            var opt = document.createElement('option');
                            opt.value = data[i]['id_rubrik'];
                            opt.innerHtml = data[i]['rincian_rubrik'];
                            select.appendChild(opt);
                        }

                    }
                });
            });
        });
    </script>

</body>

</html>