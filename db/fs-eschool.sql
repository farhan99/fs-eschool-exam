-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2021 at 03:54 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fs-eschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_guru`
--

CREATE TABLE `data_guru` (
  `id_guru` int(100) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `nama_guru` varchar(20) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_telp` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `foto_guru` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_guru`
--

INSERT INTO `data_guru` (`id_guru`, `nip`, `nama_guru`, `tempat_lahir`, `tgl_lahir`, `no_telp`, `alamat`, `foto_guru`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '882947749', 'ali budimansyah', 'jakarta', '1968-02-11', '086338393947', 'jl sudirman', 'GVT_1619 edit biru.jpg', NULL, NULL, NULL),
(2, '5793989802', 'Thomas Alfa Edison', 'pekanbaru', '1978-09-27', '0876488398', 'perawang barat', 'GVT_5247.jpg', NULL, NULL, NULL),
(3, '2234555', 'Arini Fitria', 'pekanbaru', NULL, '08848466589', 'jl umban sari', '', NULL, NULL, NULL),
(4, '12345346575', 'yuni hastuti', 'padang', NULL, '0823456786', 'pandau permai', 'GVT_5303.jpg', NULL, NULL, NULL),
(5, '8840402728', 'ahmad dahlan', 'padang ', '1967-10-10', '882734778990', 'jl siak', 'GVT_5292.jpg', NULL, NULL, NULL),
(6, '1234455', 'willy', 'pekanbaru', '1996-03-11', '08232143553', 'pandau', 'karyawan.jpg', NULL, NULL, NULL),
(7, '12345667', 'maulida sari', 'padang sidempuan', NULL, '08127571706', 'Jl. KH Nasution No.990', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_mata-pelajaran`
--

CREATE TABLE `data_mata-pelajaran` (
  `id_mapel` int(100) NOT NULL,
  `nama_mapel` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_mata-pelajaran`
--

INSERT INTO `data_mata-pelajaran` (`id_mapel`, `nama_mapel`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'matematika', NULL, NULL, NULL),
(2, 'Biologi', NULL, NULL, NULL),
(3, 'Fisika', NULL, NULL, NULL),
(4, 'Kimia', NULL, NULL, NULL),
(5, 'Bahasa Indonesia', NULL, NULL, NULL),
(6, 'Bahasa Inggris', NULL, NULL, NULL),
(7, 'Bahasa Mandarin', NULL, NULL, NULL),
(8, 'Sejarah', NULL, NULL, NULL),
(9, 'Geografi', NULL, NULL, NULL),
(10, 'Pendidikan Olahraga', NULL, NULL, NULL),
(11, 'Kewarganegaraan', NULL, NULL, NULL),
(12, 'sosiologi', NULL, NULL, NULL),
(13, 'Akuntansi', NULL, NULL, NULL),
(14, 'Ilmu Komputer dan TI', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `evaluasi`
--

CREATE TABLE `evaluasi` (
  `id_evaluasi` int(10) NOT NULL,
  `jenis_evaluasi` varchar(200) NOT NULL,
  `id_kelas_mapel` int(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `evaluasi`
--

INSERT INTO `evaluasi` (`id_evaluasi`, `jenis_evaluasi`, `id_kelas_mapel`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UH 1', 19, '2021-08-18 10:10:10', '2021-08-18 10:10:10', NULL),
(2, 'UTS', 23, '2021-08-18 10:10:10', '2021-08-18 10:10:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(100) NOT NULL,
  `nama_kelas` varchar(10) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_kelas`, `nip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'I A', '1234455', '2021-02-24 04:48:09', '2021-02-24 04:48:09', NULL),
(2, 'I B', '12345667', '2021-03-02 00:33:39', '2021-03-02 00:33:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_mapel`
--

CREATE TABLE `kelas_mapel` (
  `id_kelas_mapel` int(10) NOT NULL,
  `id_mapel` int(10) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas_mapel`
--

INSERT INTO `kelas_mapel` (`id_kelas_mapel`, `id_mapel`, `id_kelas`, `nip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 0, '2234555', NULL, NULL, NULL),
(4, 1, 0, '2234555', NULL, NULL, NULL),
(6, 3, 0, '12345667', NULL, NULL, NULL),
(8, 7, 0, '1234455', NULL, NULL, NULL),
(9, 7, 0, '1234455', NULL, NULL, NULL),
(11, 4, 1, '1234455', NULL, NULL, NULL),
(12, 2, 2, '12345667', NULL, NULL, NULL),
(15, 10, 4, '1234455', NULL, NULL, NULL),
(16, 4, 1, '2234555', NULL, NULL, NULL),
(17, 8, 4, '5793989802', NULL, NULL, NULL),
(18, 13, 3, '8840402728', NULL, NULL, NULL),
(19, 8, 1, '882947749', NULL, NULL, NULL),
(23, 10, 2, '2234555', NULL, NULL, NULL),
(24, 1, 1, '882947749', NULL, NULL, NULL),
(25, 9, 1, '8840402728', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(100) NOT NULL,
  `nis` varchar(100) NOT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_telp` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `foto_siswa` varchar(100) DEFAULT NULL,
  `spp` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nis`, `nisn`, `nama_siswa`, `tempat_lahir`, `tanggal_lahir`, `no_telp`, `alamat`, `jenis_kelamin`, `foto_siswa`, `spp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '3115864953', '3115864953', 'ADZKIA QONITA', 'Pekanbaru', '2011-07-25', '081261238121', NULL, NULL, NULL, 3, '2021-02-24 05:03:59', '2021-02-24 05:04:55', NULL),
(2, '3129164448', '3129164448', 'AISYAH AFIQAH', 'Pekanbaru', '2012-07-16', '081261238121', NULL, NULL, NULL, 1, '2021-02-24 05:07:49', '2021-02-24 05:07:49', NULL),
(3, '123', '12345', 'Budi', 'Pekanbaru', '2000-01-01', '081261238121', NULL, NULL, NULL, 1, '2021-03-02 00:34:46', '2021-03-02 00:34:46', NULL),
(4, '3129164449', '3129164449', 'Ali', 'Pekanbaru', '2000-01-01', '081261238121', NULL, NULL, NULL, 6, '2021-03-03 04:04:30', '2021-03-03 04:05:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(9) NOT NULL,
  `username` varchar(20) NOT NULL,
  `name` varchar(32) NOT NULL,
  `password` varchar(200) NOT NULL,
  `privilege` enum('admin','guru','user') NOT NULL DEFAULT 'user',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `name`, `password`, `privilege`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'admin2', 'suhen', '1q2w3e', 'admin', 1, '2021-08-15 09:18:37', '2021-08-15 09:18:37', '2021-08-15 09:18:37'),
(21, 'udin', 'Udin', '$2y$10$5jWCMqHkfN41nnsr8FUK9esaFWYU2x5PdF/18riYDOAWtPRDZ1MEy', 'user', 1, '2021-08-15 05:44:36', '2021-08-15 05:44:36', '0000-00-00 00:00:00'),
(22, 'elon', 'Pak Elon', '$2y$10$g97aSlFgolqeVP0xs9cN3uE4SL7YXh.b9yAIcDFfv6X.75H6hkp8q', 'user', 1, '2021-08-15 10:45:23', '2021-08-15 10:45:23', '0000-00-00 00:00:00'),
(23, 'fitri', 'Fitri Junika', '$2y$10$X.JVT25Sh.m2QZ29UnS/zOAZH8tQ/46oZWF7dRNenYXeRXDNloUS6', 'user', 1, '2021-08-15 11:13:45', '2021-08-15 11:13:45', '0000-00-00 00:00:00'),
(24, 'admin', 'Farhan', '$2y$10$73DICz0qKf.qPby6y4b4T.BconJZCLSXwORqSKaPF2VGz1MfOOnpW', 'user', 1, '2021-08-15 11:35:04', '2021-08-15 11:35:04', '0000-00-00 00:00:00'),
(25, 'kaka', 'Kaka', '$2y$10$ee7e.dddmKkg08fxfXDsiO5aZkzoXYVmdbAdp./8CZ08sHj/LdNPu', 'user', 1, '2021-08-15 11:40:40', '2021-08-15 11:40:40', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_guru`
--
ALTER TABLE `data_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `data_mata-pelajaran`
--
ALTER TABLE `data_mata-pelajaran`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `evaluasi`
--
ALTER TABLE `evaluasi`
  ADD PRIMARY KEY (`id_evaluasi`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kelas_mapel`
--
ALTER TABLE `kelas_mapel`
  ADD PRIMARY KEY (`id_kelas_mapel`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_guru`
--
ALTER TABLE `data_guru`
  MODIFY `id_guru` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_mata-pelajaran`
--
ALTER TABLE `data_mata-pelajaran`
  MODIFY `id_mapel` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `evaluasi`
--
ALTER TABLE `evaluasi`
  MODIFY `id_evaluasi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kelas_mapel`
--
ALTER TABLE `kelas_mapel`
  MODIFY `id_kelas_mapel` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
